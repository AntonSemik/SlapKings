using UnityEngine;
using TMPro;
public class Upgrade : MonoBehaviour
{
    [Header("Damage upgrade")]
    [SerializeField] TMP_Text damageCostText;
    [SerializeField] TMP_Text damageLevelText;
    [SerializeField] private GameObject damageCost;
    [SerializeField] private GameObject damageAdUpgrade;
    [SerializeField] private GameObject damageIcon;
    private bool adsDamageUpgradeAvailable = true;

    [Header("Health upgrade")]
    [SerializeField] TMP_Text healthCostText;
    [SerializeField] TMP_Text healthLevelText;
    [SerializeField] private GameObject healthCost;
    [SerializeField] private GameObject healthAdUpgrade;
    [SerializeField] private GameObject healthIcon;
    private bool adsHealthUpgradeAvailable = true;

    private PlayerStats playerStats = new PlayerStats();

    private void Start()
    {
        Singletons.Instance.Coins.OnChanged += OnCoinsChanged;
        Singletons.Instance.GameStateMachine.LevelComplete += ResetAds;
        Singletons.Instance.GameStateMachine.LevelFailed += ResetAds;

        UpdateUI();
    }

    private void OnCoinsChanged(int value)
    {
        UpdateUI();
    }

    private void UpdateUI()
    {
        UpdateDamageUpButton();
        UpdateHealthUpButton();
    }

    public void UpgradeHealth()
    {
        if (IsEnough(playerStats.HealthLevel))
        {
            int level = playerStats.HealthLevel + 1;
            playerStats.SaveHealthLevel(level);
            Singletons.Instance.Coins.ChangeValue(-CalculateCost(level));
            UpdateUI();

            return;
        }

        if (adsHealthUpgradeAvailable)
        {
            UpgrageForAd();
            playerStats.SaveHealthLevel(playerStats.HealthLevel + 1);

            adsHealthUpgradeAvailable = false;

            UpdateUI();
        }
    }

    public void UpgradeDamage()
    {
        if (IsEnough(playerStats.DamageLevel))
        {
            int level = playerStats.HealthLevel + 1;
            playerStats.SaveDamageLevel(level);
            Singletons.Instance.Coins.ChangeValue(-CalculateCost(level - 1));
            UpdateUI();

            return;
        }

        if (adsDamageUpgradeAvailable)
        {
            UpgrageForAd();
            playerStats.SaveDamageLevel(playerStats.DamageLevel + 1);

            adsDamageUpgradeAvailable = false;

            UpdateUI();
        }
    }

    private void UpgrageForAd()
    {
        Singletons.Instance.AdsPlaceholder.ShowDemandedAd();
    }

    private bool IsEnough(int value)
    {
        return Singletons.Instance.Coins.IsEnough(CalculateCost(value));
    }

    private void UpdateHealthUpButton()
    {
        bool isEnough = IsEnough(playerStats.HealthLevel);
        
        healthCostText.text = CalculateCost(playerStats.HealthLevel).ToString();
        healthLevelText.text = "HEALTH (" + playerStats.HealthLevel.ToString() + ")";
        healthCost.SetActive(isEnough);
        healthAdUpgrade.SetActive(!isEnough);
        healthIcon.SetActive(isEnough);
    }
    
    private void UpdateDamageUpButton()
    {
        bool isEnough = IsEnough(playerStats.DamageLevel);

        damageCostText.text = CalculateCost(playerStats.DamageLevel).ToString();
        damageLevelText.text = "DAMAGE (" + playerStats.DamageLevel.ToString() + ")";
        damageCost.SetActive(isEnough);
        damageAdUpgrade.SetActive(!isEnough);
        damageIcon.SetActive(isEnough);
    }

    private int CalculateCost(int _level)
    {
        return PlayerStats.StartCost + PlayerStats.CostProgressPerLevel * _level;
    }

    private void ResetAds()
    {
        adsDamageUpgradeAvailable = true;
        adsHealthUpgradeAvailable = true;
    }
}